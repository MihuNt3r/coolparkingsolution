﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Models;

namespace ConsoleClient
{
    public class Menu
    {
        private readonly HttpHandler handler;

        public Menu()
        {
            handler = new HttpHandler();
        }

        public async Task Start()
        {
            int a = 0;
            while (a != 10)
            {
                Console.WriteLine("Available commands:\n1.Show current parking balance\n2.Show money earned in the current period\n3.Show occupied/unoccupied parking spaces\n4.Show all transactions of the current period\n5.Show transactions history\n6.Show list of vehicles\n7.Add vehicle to a parking\n8.Remove vehicle from parking\n9.Top up vehicle balance\n0.Exit\nEnter number of command:");
                int cmd = Int32.Parse(Console.ReadLine());
                switch (cmd)
                {
                    case (1):
                        await handler.GetBalance();
                        break;
                    case (2):
                        List<TransactionInfo> list = await handler.GetLastTransactions();
                        decimal sum = list.Sum(transaction => transaction.Sum);
                        Console.WriteLine(($"Money earned in the current period: {sum}$"));
                        break;
                    case (3):
                        await handler.GetFreePlaces();
                        break;
                    case (4):
                        await handler.GetLastTransactions();
                        break;
                    case (5):
                        await handler.GetAllTransactions();
                        break;
                    case (6):
                        await handler.GetVehicles();
                        break;
                    case (7):
                        await AddVehicle();
                        break;
                    case (8):
                        await RemoveVehicle();
                        break;
                    case (9):
                        await TopUpVehicle();
                        break;
                    case (0):
                        a = 10;
                        break;
                    default:
                        Console.WriteLine("Error. Try again");
                        break;

                }
            }
        }

        private async Task AddVehicle()
        {
            VehicleType type = default;
            bool isInputValid = false;
            do
            {
                Console.WriteLine("Enter type of a vehicle: 1 - Passenger Car, 2 - Truck, 3 - Bus, 4 - Motorcycle");
                int cmd = Int32.Parse(Console.ReadLine());
                switch (cmd)
                {
                    case (1):
                        type = VehicleType.PassengerCar;
                        isInputValid = true;
                        break;
                    case (2):
                        type = VehicleType.Truck;
                        isInputValid = true;
                        break;
                    case (3):
                        type = VehicleType.Bus;
                        isInputValid = true;
                        break;
                    case (4):
                        type = VehicleType.Motorcycle;
                        isInputValid = true;
                        break;
                    default:
                        Console.WriteLine("Error. Try again");
                        break;
                }
            } while (!isInputValid);

            try
            {
                Console.WriteLine("Enter Id of a vehicle(Format of id XX-YYYY-XX): ");
                string id = Console.ReadLine();

                Console.WriteLine("Enter starting balance: ");
                decimal balance = Decimal.Parse(Console.ReadLine());

                await handler.PostVehicle(new Vehicle(id, type, balance));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task RemoveVehicle()
        {
            Console.WriteLine("Enter id of a vehicle");
            string id = Console.ReadLine();
            await handler.DeleteVehicle(id);
        }

        private async Task TopUpVehicle()
        {
            try
            {
                Console.WriteLine("Enter id of a vehicle");
                string id = Console.ReadLine();
                Console.WriteLine("Enter top up sum");
                decimal sum = Decimal.Parse(Console.ReadLine());

                TopUp topUp = new TopUp(id, sum);
                await handler.TopUpVehicle(topUp);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
