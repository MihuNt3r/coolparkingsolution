﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using Newtonsoft.Json;

namespace ConsoleClient
{
    public class HttpHandler
    {
        private HttpClient client;

        public HttpHandler()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44337/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task GetBalance()
        {
            var action = "api/parking/balance";

            HttpResponseMessage response = await client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                int balance = await response.Content.ReadFromJsonAsync<int>();
                Console.WriteLine($"Balance: {balance}$");
            }
            else
            {
                string Text = await response.Content.ReadAsStringAsync();
                Console.WriteLine(Text);
            }
        }

        public async Task GetCapacity()
        {
            var action = "api/parking/capacity";

            HttpResponseMessage response = await client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                int capacity = await response.Content.ReadFromJsonAsync<int>();
                Console.WriteLine("Capacity: " + capacity);
            }
            else
            {
                string Text = await response.Content.ReadAsStringAsync();
                Console.WriteLine(Text);
            }
        }

        public async Task GetFreePlaces()
        {
            var action = "api/parking/freePlaces";

            HttpResponseMessage response = await client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                int freePlaces = await response.Content.ReadFromJsonAsync<int>();

                Console.WriteLine("Free places: " + freePlaces);
            }
            else
            {
                string Text = await response.Content.ReadAsStringAsync();
                Console.WriteLine(Text);
            }
        }

        public async Task GetVehicles()
        {
            var action = "api/vehicles";

            HttpResponseMessage response = await client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                List<Vehicle> vehicles = await response.Content.ReadFromJsonAsync<List<Vehicle>>();
                vehicles.ForEach(Console.WriteLine);
            }
            else
            {
                string Text = await response.Content.ReadAsStringAsync();
                Console.WriteLine(Text);
            }
        }

        public async Task GetVehicleById(string id)
        {
            var action = $"api/vehicles/{id}";
            HttpResponseMessage response = await client.GetAsync(action);

            if (response.IsSuccessStatusCode)
            {
                Vehicle vehicle = await response.Content.ReadFromJsonAsync<Vehicle>();
                Console.WriteLine(vehicle);
            }
            else
            {
                string Text = await response.Content.ReadAsStringAsync();
                Console.WriteLine(Text);
            }
        }

        public async Task DeleteVehicle(string id)
        {
            var action = $"api/vehicles/{id}";
            HttpResponseMessage response = await client.DeleteAsync(action);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.ReasonPhrase);
            }
            else
            {
                string Text = await response.Content.ReadAsStringAsync();
                Console.WriteLine(Text);
            }
        }

        public async Task PostVehicle(Vehicle vehicle)
        {
            var action = "api/vehicles";
            var jsonVehicle = JsonConvert.SerializeObject(vehicle);
            var data = new StringContent(jsonVehicle, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(action, data);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.ReasonPhrase);
            }
            else
            {
                string Text = await response.Content.ReadAsStringAsync();
                Console.WriteLine(Text);
            }
        }

        public async Task<List<TransactionInfo>> GetLastTransactions()
        {
            var action = "api/transactions/last";

            HttpResponseMessage response = await client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                List<TransactionInfo> list = await response.Content.ReadFromJsonAsync<List<TransactionInfo>>();
                return list;
            }

            string Text = await response.Content.ReadAsStringAsync();
            Console.WriteLine(Text);
            return null;
        }

        public async Task GetAllTransactions()
        {
            var action = "api/transactions/all";

            HttpResponseMessage response = await client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                string transactions = await response.Content.ReadFromJsonAsync<string>();
                Console.WriteLine(transactions);
            }
            else
            {
                string Text = await response.Content.ReadAsStringAsync();
                Console.WriteLine(Text);
            }
        }

        public async Task TopUpVehicle(TopUp topUp)
        {
            var action = "api/transactions/topUpVehicle";
            var jsonTopUp = JsonConvert.SerializeObject(topUp);
            var data = new StringContent(jsonTopUp, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync(action, data);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.ReasonPhrase);
            }
            else
            {
                string Text = await response.Content.ReadAsStringAsync();
                Console.WriteLine(Text);
            }
        }
    }
}
