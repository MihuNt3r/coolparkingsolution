﻿using System;
using System.Threading.Tasks;

namespace ConsoleClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Menu menu = new Menu();
            await menu.Start();
        }
    }
}
