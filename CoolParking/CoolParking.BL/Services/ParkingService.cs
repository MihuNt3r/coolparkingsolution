﻿using System;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking parking;
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;
        private readonly ILogService logService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.parking = Parking.GetInstance();
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            this.logService = logService;

            this.withdrawTimer.Elapsed += Withdraw; // Add handler for withdrawing
            this.logTimer.Elapsed += WriteToLog; // Add handler for logging

            this.withdrawTimer.Start();
            this.logTimer.Start();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (parking.Vehicles.Count >= Settings.parkingCapacity)
                throw new InvalidOperationException("Parking is full");

            if (parking.Vehicles.Count != 0 && parking.Vehicles.Any(v => vehicle.Id == v.Id))
                throw new ArgumentException("Id already exists");

            parking.Vehicles.Add(vehicle);
        }

        public void Dispose()
        {
            withdrawTimer.Dispose();
            logTimer.Dispose();
            parking.Dispose();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.parkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.parkingCapacity - parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return parking.LastParkingTransactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parking.Vehicles.AsReadOnly();
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = parking.Vehicles.Find(vehicle => vehicle.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException("Can't find vehicle with such id");

            if (vehicle != null && vehicle.Balance < 0)
                throw new InvalidOperationException("Cannot remove a vehicle, because it has a debt");

            parking.Vehicles.Remove(vehicle);
        }

        private decimal CalculateWithdrawSum(Vehicle vehicle)
        {
            decimal tariff = Settings.tariffs[vehicle.VehicleType];
            decimal withdrawSum;

            if (vehicle.Balance > 0 && vehicle.Balance >= tariff)// if balance is greater than 0 and greater than tariff just assign tariff to withdrawSum
            {
                withdrawSum = tariff;
            }
            else if (vehicle.Balance <= 0)// if balance is 0 or lower - calculate tariff with fineCoefficient multiplier
            {
                withdrawSum = tariff * Settings.fineCoefficient;
            }
            else// if balance is greater than 0 but lower than tariff - calculate remainder with fineCoefficient multiplier
            {
                decimal remainder = tariff - vehicle.Balance;
                remainder *= Settings.fineCoefficient;
                withdrawSum = vehicle.Balance + remainder;
            }

            return withdrawSum;
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Invalid topup sum");

            var vehicle = parking.Vehicles.Find(vehicle => vehicle.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException("Can't find vehicle with such number");

            vehicle.Balance += sum;
        }

        // Handler method for withdrawing 
        private void Withdraw(Object source, ElapsedEventArgs e)
        {
            if (parking.Vehicles.Count != 0)
            {
                foreach (Vehicle vehicle in parking.Vehicles)
                {
                    decimal withdrawSum = CalculateWithdrawSum(vehicle);
                    vehicle.Balance -= withdrawSum;
                    parking.Balance += withdrawSum;
                    TransactionInfo info = new TransactionInfo(DateTime.Now, vehicle.Id, withdrawSum);
                    parking.LastParkingTransactions.Add(info);
                }
            }
        }
        // Handler method for writing to log
        private void WriteToLog(Object source, ElapsedEventArgs e)
        {
            TransactionInfo[] lastTransactions = GetLastParkingTransactions(); // Retrieve last transactions
            string logInfo = null;
            for (int i = 0; i < lastTransactions.Length; i++)
            {
                logInfo += lastTransactions[i].ToString() + "\n";
            }
            logService.Write(logInfo); // Write last transactions to log
            parking.LastParkingTransactions.Clear(); // Clear transaction list after writing to log
        }
    }
}