﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Timer Timer;

        public double Interval
        {
            get
            {
                return Timer.Interval;
            }
            set
            {
                Timer.Interval = value;
            }
        }

        public TimerService()
        {
            this.Timer = new Timer();
        }

        public event ElapsedEventHandler Elapsed
        {
            add { Timer.Elapsed += value; }
            remove { Timer.Elapsed -= value; }
        }

        public void Dispose()
        {
            Timer.Dispose();
        }

        public void Start()
        {
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Stop();
        }
    }
}