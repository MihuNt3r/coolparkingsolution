﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }
        public LogService(string logFilePath)
        {
            LogPath = logFilePath;
        }
        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("File not exists");
            string res;
            using (StreamReader file = new StreamReader(LogPath))
            {
                res = file.ReadToEnd();
            }
            return res;
        }

        public void Write(string logInfo)
        {
            using (StreamWriter streamWriter = new StreamWriter(LogPath, true))
            {
                streamWriter.WriteLine(logInfo);
            }
        }
    }
}