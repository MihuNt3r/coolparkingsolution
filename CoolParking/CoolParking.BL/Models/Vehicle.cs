﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonProperty("id")]
        public string Id { get; }
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; }
        [JsonProperty("balance")]
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            // Validation
            if (!Settings.regex.IsMatch(id))
                throw new ArgumentException("Invalid id number");
            if (balance < 0)
                throw new ArgumentException("Invalid balance");


            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber(string id)
        {
            return new Guid(id).ToString();
        }

        public override string ToString()
        {
            return String.Format("Vehicle: {0} ID: {1} Balance: {2}$", VehicleType.ToString(), Id, Balance);
        }
    }
}