﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;


namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal startingBalance = 0;
        public static int parkingCapacity = 10;
        public static int payInterval = 5; // seconds
        public static int logInterval = 60; // seconds

        public static Dictionary<VehicleType, decimal> tariffs = new()
        {
            { VehicleType.PassengerCar, 2 },
            { VehicleType.Truck, 5 },
            { VehicleType.Bus, 3.5m },
            { VehicleType.Motorcycle, 1 }
        };

        public static decimal fineCoefficient = 2.5m;
        public static string logFilePath = Path.Combine(Environment.CurrentDirectory, "Transactions.log");
        public static Regex regex = new Regex("^[A-Z]{2}[-][0-9]{4}[-][A-Z]{2}$");
    }
}