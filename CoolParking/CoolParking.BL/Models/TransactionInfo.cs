﻿using System;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; }
        [JsonProperty("sum")]
        public decimal Sum { get; }
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; }

        public TransactionInfo(DateTime transactionDate, string vehicleId, decimal sum)
        {
            TransactionDate = transactionDate;
            VehicleId = vehicleId;
            Sum = sum;
        }
        public override string ToString()
        {
            return $"{TransactionDate}: {Sum} money withdrawn from vehicle with Id='{VehicleId}'.";
        }
    }
}