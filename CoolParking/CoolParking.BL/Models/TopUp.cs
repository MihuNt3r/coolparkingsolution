﻿using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class TopUp
    {
        [JsonProperty("id")]
        public string Id { get; }
        [JsonProperty("Sum")]
        public decimal Sum { get; }

        public TopUp(string id, decimal sum)
        {
            Id = id;
            Sum = sum;
        }
    }
}