﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        public decimal Balance { get; internal set; }
        public List<Vehicle> Vehicles { get; }
        public List<TransactionInfo> LastParkingTransactions { get; private set; }

        private static Parking instance;

        private Parking()
        {
            Balance = Settings.startingBalance;
            Vehicles = new List<Vehicle>();
            LastParkingTransactions = new List<TransactionInfo>();
        }

        public static Parking GetInstance()
        {
            if (instance == null)
                instance = new Parking();

            return instance;
        }

        public void Dispose()
        {
            if (instance != null)
            {
                instance.Balance = 0;
                instance.Vehicles.Clear();
                instance.LastParkingTransactions.Clear();
            }
        }
    }
}