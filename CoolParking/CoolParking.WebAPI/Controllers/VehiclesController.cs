﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService parkingService;
        public VehiclesController(IParkingService service)
        {
            parkingService = service;
        }

        [HttpGet]
        public ActionResult GetVehicles()
        {
            return Ok(parkingService.GetVehicles());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult GetVehicleById(string id)
        {
            if (!Settings.regex.IsMatch(id))
                return BadRequest("Invalid id");

            Vehicle vehicle = parkingService.GetVehicles().ToList().Find(v => v.Id == id);

            if (vehicle == null)
                return NotFound("Can't find car with such id");

            return Ok(vehicle);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Vehicle> PostVehicle(Vehicle vehicle)
        {
            try
            {
                parkingService.AddVehicle(vehicle);
                return Created("", vehicle);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult RemoveVehicle(string id)
        {
            if (!Settings.regex.IsMatch(id))
                return BadRequest("Invalid id");

            try
            {
                parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}