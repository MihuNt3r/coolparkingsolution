﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Http;

namespace CoolParking.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;
        public ParkingController(IParkingService service)
        {
            parkingService = service;
        }

        [HttpGet("balance")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(parkingService.GetBalance());
        }

        [HttpGet("capacity")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<int> GetCapacity()
        {
            return Ok(parkingService.GetCapacity());
        }

        [HttpGet("freePlaces")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(parkingService.GetFreePlaces());
        }
    }
}
