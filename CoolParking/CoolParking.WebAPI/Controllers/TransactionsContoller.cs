﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService parkingService;
        public TransactionsController(IParkingService service)
        {
            parkingService = service;
        }

        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionInfo>> GetLastTransactions()
        {
            var lastTransactions = parkingService.GetLastParkingTransactions();
            return Ok(lastTransactions);
        }

        [HttpGet("all")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<string> GetTransactionsFromLog()
        {
            try
            {
                string transactions = parkingService.ReadFromLog();
                return Ok(transactions);
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut("topUpVehicle")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult TopUpVehicle([FromBody] TopUp topUp)
        {
            if (!Settings.regex.IsMatch(topUp.Id) || topUp.Sum <= 0)
                return BadRequest();

            var vehicle = parkingService.GetVehicles().ToList().Find(v => v.Id == topUp.Id);

            if (vehicle == null)
                return NotFound("Can't found vehicle with such id");

            parkingService.TopUpVehicle(topUp.Id, topUp.Sum);
            return Ok(vehicle);
        }
    }
}